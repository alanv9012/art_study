const express = require('express');
const multer  = require('multer');
const upload = multer({ dest: './public/images' });
const app = express();
const path = require('path');
const router = express.Router();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Images = require('./models/Images');
const Tags = require('./models/Tags');
const fs = require('fs');
const Practices = require('./models/Practices');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static('public'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.set('views', process.cwd()+'/views');
console.log(process.cwd());
router.get('/', (req,res) => {
    res.render('index');
});

router.get('/addImg', (req,res) => {
    res.render('addImg');
});

router.get('/addPractice', (req, res) => {
    res.render('addPractice');
});

router.get('/getTags', (req, res) => {
    Tags.find().then(tags => res.json(tags));
});

router.get('/getPractices', (req, res) => {
    Practices.find().then(practices => res.json(practices));
});

router.get('/practice/:id', (req, res) => {
    Practices.findOne({_id:req.params.id}).then(practice => {
        Images.find({tags: {$in: practice.tags},$sample: {size: practice.quantity }}).limit(practice.quantity).then(images => {
            console.log(images.length);
            res.render('slide',{practice, images});
        })
    });
});
//esta funcion agrega una nueva imagen al final del arreglo, se trata de agregar el objeto que se pasa desde addimg.js, actualmente no funcionaXD
router.post('/createImage', upload.single('file'), (req,res) => {
    const body = req.body;
    const file = req.file;
    const tagArray = body.tags.split(',').map(t => t.replace(/\s/g, ''));
    const img = fs.readFileSync(req.file.path);
    const Image = new Images({ file: Buffer.from(img), mimetype: file.mimetype, tags: tagArray });
    Tags.find({
        name: {
            $in: tagArray
        }
    }).then(tags => {
        const sanTags = tags.map(t => t.name);
        tagArray.forEach(bodyTag => {
            if(bodyTag !== '' && !sanTags.includes(bodyTag)) {
                new Tags({name:bodyTag}).save();
            }

        })
    })
    Image
        .save()
        .then(() => {
            res.json({message: "ta bien"});
        })
        .catch((e) => {
            res.json({message: "ta mal", error: e});
        });
});


router.post('/createPractice', (req, res) => {
    console.log(req.body)
    req.body.tags = req.body.tags.split(',').map(t => t.replace(/\s/g, ''));
    const practice = new Practices(req.body)
        .save()
        .then(() => {
            res.json({message: "ta bien"});
        });
});


//aqui se lanza el programa en localhost 3000
app.use('/', router);
app.listen(process.env.port || 3000, () => {
    console.log('running... 3000')
    mongoose.connect('mongodb://localhost:27017/quickdraw');
});

