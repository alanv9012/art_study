const mongoose = require('mongoose');

const Practices = mongoose.model('Practices', { name: String, duration: Number, tags: [String], quantity: Number });
module.exports =  Practices;
