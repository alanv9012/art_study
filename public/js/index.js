//this is a very cool part of the code, it takes the JSON file and parses the whole array
//allowing to create a bunch of instances of buttons, and when pressed it sends us to the Slider
//page as well as it passes the practice id to the next js file, this sistem will
//also be used by images to save tags
// in the practices json we can see how a practice is saved as an object

(function() {
  fetch('getPractices')
  .then(data => data.json())
  .then(practices => {
    document.getElementById("buttons").innerHTML = `
      ${practices.map(function(practice){
        return `
        <div>
        <button type = "button" onclick = window.location.href='practice/${practice._id}';>
        ${practice.name}</button>
        </div>
        `
      }).join('')}
    `
  })
})()
