const  addPractice = (ev)=>{
  ev.preventDefault();

  const name = document.getElementById("name").value;
  const tags = document.getElementById("tags").value;
  const duration = document.getElementById("duration").value;
  const quantity = document.getElementById("quantity").value;
  
  const body = {name,tags,duration,quantity};

  //con eso se puede mandar el objeto que se crea en base a lo que se ve en addimg.html
  fetch('/createPractice', {
      method: 'POST', 
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body) 
    })
    .then(data => data.json())
    .then(json =>{
      console.log("good",json);
    })
    .catch(error =>{
        console.log(error);
    });

  
}
document.addEventListener('DOMContentLoaded', ()=>{
  document.getElementById('submit').addEventListener('click', addPractice)
});