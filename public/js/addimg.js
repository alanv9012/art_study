const  addImg = (ev)=>{
    ev.preventDefault();
    const image = document.getElementById("source");
    const tags = document.getElementById("tags").value;
    const formData = new FormData();
    formData.append('file', image.files[0]);
    formData.append('tags', tags);

    //con eso se puede mandar el objeto que se crea en base a lo que se ve en addimg.html
    fetch('/createImage', {
        method: 'POST', 
        // headers: {
        //   'Content-Type': 'application/json'
        // },
        body: formData 
      })
      .then(data => data.json())
      .then(json =>{
        console.log("good",json);
      })
      .catch(error =>{
          console.log(error);
      });
  

    
}
document.addEventListener('DOMContentLoaded', ()=>{
    document.getElementById('submit').addEventListener('click', addImg)
});