const mongoose = require('mongoose');

const Images = mongoose.model('Images', { tags: [String], file: Buffer, mimetype: String });
module.exports =  Images;
